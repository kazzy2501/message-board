class MessagesController < ApplicationController
  before_action :set_message, only: [:edit, :update, :destroy] #set_messageメゾッドをedit,update,destroy前に行う処理
  def index
    #Message を全て取得する。
    @messages = Message.all
    @message = Message.new
  end
  
  def edit
    #editアクションでは、編集画面に来た時に実行されるアクションです。
    #中の実装は空ですが、フィルターの機能によって@messageが設定された状態でedit.html.erbをレンダリングします
  end
  
  def update
    #updateアクションではbefore_actionにより設定された@messageを用いて更新処理を実装しています
    if @message.update(message_params)
      #保存成功の場合topへリダイレクト
      redirect_to root_path, notice: 'メッセージを編集しました'
    else
      #保存に失敗
      render 'edit'
    end
  end
  
  def destroy
    @message.destroy
    redirect_to root_path, notice: 'メッセージを削除しました'
  end

##ここから記述
  def create
    @message = Message.new(message_params)
   if @message.save
    redirect_to root_path , notice: 'メッセージを保存しました'
   else
    #メッセージを保存できなかった時
    @messages = Message.all
    flash.now[:alert] = "メッセージの保存に失敗しました"
    render 'index'
   end
  end

##ここから下はprivateメゾッド
  private
  def message_params
    #params[:message]のパラメータでname, bodyのみ許可する
    #返り値はex:){name: "入力されたname" , body: "入力されたbody"}
    params.require(:message).permit(:name, :body, :age)
  end
  
  def set_message
    @message = Message.find(params[:id])
  end
  ##ここまで
end