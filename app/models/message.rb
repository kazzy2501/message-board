class Message < ActiveRecord::Base
    #名前は必須＆＆20文字以内
    validates :name, length:{ maximum: 20} , presence: true
    #内容は必須＆＆2文字以上3030文字以下
    validates :body, length:{ minimum: 2, maximum: 30} , presence: true
    #年齢は数字のみ入力可、0以上
    validates :age, numericality: {only_integer: true, greater_than_or_equal_to: 0}
end
